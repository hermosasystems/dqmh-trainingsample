﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)X!!!*Q(C=\&gt;;\D&gt;N1%)8B)W-$J]I&gt;,!3(DHR+M+,.V=+UI*3B7BD!&amp;;A&amp;N4!NK!7V10_]'ADW0LS"(\#"*@&gt;3V#(PX)^0L43G^^+&gt;ZN=G`84[&lt;POS/N]74\9P'Z;&amp;ZO@\T\=;DYM_\@`=^O[LF]:`S@^,B`]WP5X``43^_B$]]?&gt;A&gt;8VKZ\FI31O;LY^UE2&gt;ZE2&gt;ZE2&gt;ZEC&gt;ZEC&gt;ZEC&gt;ZE!&gt;ZE!&gt;ZE!?ZS5VO=J/&lt;X.?8SCRSE9N=(K_29P"CI+*I5;$9'9K+2?%J0)7H]0"6B;@Q&amp;*\#5XD9297H]"3?QF.Y[+&lt;#5XA+4_%J0*1;EBIT/:\#1XEFHM34?"*0YG&amp;)*:Y%E!S7&amp;%[+Q&amp;#S-6F*0)EH]&lt;#KR*.Y%E`C34RM6O**0)EH]31?OISTEE/TT/2Y++0!%XA#4_!*0*27Y!E]A3@Q""['5_!*0!%C'$!I$E&amp;"JW#(Y%PA#4R]+0!%HM!4?!)0G]96CH&amp;G&amp;MUSE_-R(O-R(O-R(EL)?)T(?)T(?#ALYT%?YT%?YW%I'9`R')_"G%%:8K;9[7BW-I(R]$@O&amp;I_LF%0C-;N`D];.KLY"V4?7_I:2XQDK#[S_=/I,ID\2[B/I0D(K!V9@C"KI(FB&gt;5,WD,CT0N"0N3$P1^L1&gt;&lt;5P&lt;U.:,V^_]Y_6SU@F]VOFUUP&amp;YV/&amp;QU([`VW[XUX;\V7;TU8K^PLU'\JFP,Y4L?WFC@@,8V@2QPZK_@/,TYWL[`''VZ(`B^`E@?$@KX1``U8#/PA%UTC8[!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="NI_IconEditor" Type="Str">49 57 48 49 56 48 48 48 13 0 0 0 0 1 23 21 76 111 97 100 32 38 32 85 110 108 111 97 100 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 9 0 0 4 43 1 100 0 0 80 84 72 48 0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 15 13 76 97 121 101 114 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 7 0 0 3 188 0 0 0 0 0 0 0 0 0 0 3 162 0 40 0 0 3 156 0 0 3 96 0 0 0 0 0 9 0 32 0 24 0 0 0 0 0 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 255 255 255 255 0 0 0 6 66 97 110 110 101 114 100 1 0 0 0 0 0 3 67 70 71 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 11 83 109 97 108 108 32 70 111 110 116 115 0 1 8 1 1
</Property>
	<Item Name="Public API" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Arguments" Type="Folder">
			<Item Name="Request" Type="Folder">
				<Item Name="Stop Argument--cluster.ctl" Type="VI" URL="../Stop Argument--cluster.ctl"/>
				<Item Name="Get Module Execution Status Argument--cluster.ctl" Type="VI" URL="../Get Module Execution Status Argument--cluster.ctl"/>
				<Item Name="Show Panel Argument--cluster.ctl" Type="VI" URL="../Show Panel Argument--cluster.ctl"/>
				<Item Name="Hide Panel Argument--cluster.ctl" Type="VI" URL="../Hide Panel Argument--cluster.ctl"/>
				<Item Name="Show Diagram Argument--cluster.ctl" Type="VI" URL="../Show Diagram Argument--cluster.ctl"/>
				<Item Name="Get Config-RW Argument--cluster.ctl" Type="VI" URL="../Get Config-RW Argument--cluster.ctl"/>
				<Item Name="Get Config-RW (Reply Payload)--cluster.ctl" Type="VI" URL="../Get Config-RW (Reply Payload)--cluster.ctl"/>
			</Item>
			<Item Name="Broadcast" Type="Folder">
				<Item Name="Did Init Argument--cluster.ctl" Type="VI" URL="../Did Init Argument--cluster.ctl"/>
				<Item Name="Status Updated Argument--cluster.ctl" Type="VI" URL="../Status Updated Argument--cluster.ctl"/>
				<Item Name="Error Reported Argument--cluster.ctl" Type="VI" URL="../Error Reported Argument--cluster.ctl"/>
			</Item>
		</Item>
		<Item Name="Requests" Type="Folder">
			<Item Name="Show Panel.vi" Type="VI" URL="../Show Panel.vi"/>
			<Item Name="Hide Panel.vi" Type="VI" URL="../Hide Panel.vi"/>
			<Item Name="Stop Module.vi" Type="VI" URL="../Stop Module.vi"/>
			<Item Name="Get Module Execution Status.vi" Type="VI" URL="../Get Module Execution Status.vi"/>
			<Item Name="Show Diagram.vi" Type="VI" URL="../Show Diagram.vi"/>
			<Item Name="Get Config-RW.vi" Type="VI" URL="../Get Config-RW.vi"/>
		</Item>
		<Item Name="Start Module.vi" Type="VI" URL="../Start Module.vi"/>
		<Item Name="Synchronize Module Events.vi" Type="VI" URL="../Synchronize Module Events.vi"/>
		<Item Name="Obtain Broadcast Events for Registration.vi" Type="VI" URL="../Obtain Broadcast Events for Registration.vi"/>
	</Item>
	<Item Name="Broadcasts" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Broadcast Events--cluster.ctl" Type="VI" URL="../Broadcast Events--cluster.ctl"/>
		<Item Name="Obtain Broadcast Events.vi" Type="VI" URL="../Obtain Broadcast Events.vi"/>
		<Item Name="Destroy Broadcast Events.vi" Type="VI" URL="../Destroy Broadcast Events.vi"/>
		<Item Name="Module Did Init.vi" Type="VI" URL="../Module Did Init.vi"/>
		<Item Name="Status Updated.vi" Type="VI" URL="../Status Updated.vi"/>
		<Item Name="Error Reported.vi" Type="VI" URL="../Error Reported.vi"/>
		<Item Name="Module Did Stop.vi" Type="VI" URL="../Module Did Stop.vi"/>
		<Item Name="Update Module Execution Status.vi" Type="VI" URL="../Update Module Execution Status.vi"/>
		<Item Name="Get Config-B.vi" Type="VI" URL="../Get Config-B.vi"/>
	</Item>
	<Item Name="Requests" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Request Events--cluster.ctl" Type="VI" URL="../Request Events--cluster.ctl"/>
		<Item Name="Obtain Request Events.vi" Type="VI" URL="../Obtain Request Events.vi"/>
		<Item Name="Destroy Request Events.vi" Type="VI" URL="../Destroy Request Events.vi"/>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="Close Module.vi" Type="VI" URL="../Close Module.vi"/>
		<Item Name="Config Settings--cluster.ctl" Type="VI" URL="../Config Settings--cluster.ctl"/>
		<Item Name="Get Module Main VI Information.vi" Type="VI" URL="../Get Module Main VI Information.vi"/>
		<Item Name="Handle Exit.vi" Type="VI" URL="../Handle Exit.vi"/>
		<Item Name="Hide VI Panel.vi" Type="VI" URL="../Hide VI Panel.vi"/>
		<Item Name="Init Module.vi" Type="VI" URL="../Init Module.vi"/>
		<Item Name="Module Data--cluster.ctl" Type="VI" URL="../Module Data--cluster.ctl"/>
		<Item Name="Module Name--constant.vi" Type="VI" URL="../Module Name--constant.vi"/>
		<Item Name="Module Not Running--error.vi" Type="VI" URL="../Module Not Running--error.vi"/>
		<Item Name="Module Not Stopped--error.vi" Type="VI" URL="../Module Not Stopped--error.vi"/>
		<Item Name="Module Not Synced--error.vi" Type="VI" URL="../Module Not Synced--error.vi"/>
		<Item Name="Module Timeout--constant.vi" Type="VI" URL="../Module Timeout--constant.vi"/>
		<Item Name="Open VI Panel.vi" Type="VI" URL="../Open VI Panel.vi"/>
		<Item Name="Request and Wait for Reply Timeout--error.vi" Type="VI" URL="../Request and Wait for Reply Timeout--error.vi"/>
		<Item Name="RW Config File.vi" Type="VI" URL="../RW Config File.vi"/>
	</Item>
	<Item Name="Module Sync" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Destroy Sync Refnums.vi" Type="VI" URL="../Destroy Sync Refnums.vi"/>
		<Item Name="Get Sync Refnums.vi" Type="VI" URL="../Get Sync Refnums.vi"/>
		<Item Name="Synchronize Caller Events.vi" Type="VI" URL="../Synchronize Caller Events.vi"/>
		<Item Name="Wait on Event Sync.vi" Type="VI" URL="../Wait on Event Sync.vi"/>
		<Item Name="Wait on Module Sync.vi" Type="VI" URL="../Wait on Module Sync.vi"/>
	</Item>
	<Item Name="Main.vi" Type="VI" URL="../Main.vi">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
</Library>
